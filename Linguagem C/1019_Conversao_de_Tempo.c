#include <stdio.h>
 
int main() {
 
    int seg;
    scanf("%d", &seg);
    printf("%d:%d:%d\n", seg/3600, (seg%3600)/60, (seg%3600)%60);
 
    return 0;
}
